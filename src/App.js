import Header from './components/Header'
import Nav from './components/Nav'
import List from './components/List'
import Aside from './components/Aside'
import { content } from './assets/articles'

function App() {
    return (
        <div className="container px-0 md:px-4 mx-auto">
            <Header />
            <Nav />
            <div className="flex flex-col md:flex-row">
                <List articles={content} />
                <Aside
                    img="https://cdn.pixabay.com/photo/2014/08/18/12/42/glider-420720__480.jpg"
                    alt="glider"
                />
            </div>
        </div>
    )
}

export default App
