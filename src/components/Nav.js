const NavItem = ({ label, color }) => {
    return (
        <li>
            <button
                className={`px-4 py-2 rounded-md cursor-pointer text-gray-500 border border-transparent 0 hover:text-${color}-500 hover:border-${color}-600`}
            >
                {label}
            </button>
        </li>
    )
}

const Nav = () => {
    return (
        <nav className="flex flex-col md:flex-row justify-between p-4 font-Poppins text-sm font-medium shadow-md">
            <div>
                <ul className="flex justify-center space-x-2">
                    <NavItem label="Featured" color="yellow" />
                    <NavItem label="Popular" color="blue" />
                    <NavItem label="Recent" color="green" />
                    <li></li>
                </ul>
            </div>
            <div className="w-full md:w-1/4">
                <input
                    type="text"
                    placeholder="Search"
                    className="py-3 px-4 bg-white rounded-lg placeholder-gray-300 text-gray-500 appearance-none inline-block w-full shadow-md focus:outline-none focus:ring-2 focus:ring-fridge"
                />
            </div>
        </nav>
    )
}

export default Nav
