import logo from '../assets/images/tailwind-logo.svg'

const Header = () => {
    return (
        <header className="flex mx-auto font-cursive bg-gradient-to-b from-icy via-pink-500 to-blue-200 border-solid border-b-2 border-fridge">
            <div className="p-4 ">
                <div>
                    <img
                        className="w-12 h-auto mb-3 inline-block"
                        src={logo}
                        alt="tailwind logo"
                    />
                    <h1 className="inline-block font-Poppins text-dawn ml-2">
                        Tailwindcss
                    </h1>
                </div>
                <p className="font-Amatic text-fridge -mt-3">
                    A utility-first CSS framework packed with classes
                </p>
            </div>
        </header>
    )
}

export default Header
