const Aside = ({ img, alt }) => {
    return (
        <div className="w-full md:w-1/3 bg-yellow-400">
            <img
                className="overflow-hidden h-full w-auto"
                src={img}
                alt={alt}
            />
        </div>
    )
}

export default Aside
