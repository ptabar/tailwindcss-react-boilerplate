import ListItem from './ListItem'

const List = ({ articles }) => {
    return (
        <div className="w-full md:w-2/3">
            {articles.map((article) => (
                <ListItem key={article.title} article={article} />
            ))}
        </div>
    )
}

export default List
