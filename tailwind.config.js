// tailwind.config.js
const colors = require('tailwindcss/colors')

module.exports = {
    purge: ['./src/**/*.{js,jsx,ts,tsx}', './public/index.html'],
    darkMode: false, // or 'media' or 'class'
    theme: {
        extend: {},
        fontFamily: {
            sans: ['Tahoma', 'Helvetica'],
            serif: ['Garamond', 'Georgia'],
            mono: ['Courier New', 'SFMono-Regular'],
            Amatic: ['"Amatic SC"', 'cursive'],
            Poppins: ['Poppins', 'sans-serif'],
        },
        container: {
            padding: {
                DEFAULT: '1rem',
                sm: '2rem',
                lg: '4rem',
                xl: '5rem',
                '2xl': '6rem',
            },
        },
        colors: {
            icy: '#ebf7f7',
            fridge: '#06B6D4',
            dawn: '#173361',
            transparent: 'transparent',
            current: 'currentColor',
            black: colors.black,
            white: colors.white,
            gray: colors.trueGray,
            indigo: colors.indigo,
            red: colors.rose,
            yellow: colors.amber,
            green: colors.green,
            blue: colors.blue,
        },
    },
    variants: {
        extend: {},
    },
    plugins: [],
}
